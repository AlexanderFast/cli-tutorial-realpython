"""Simple command line interface (cli) for ls linux command

Not using the argpase library. 
"""
import os       # Operationg system dependent functionality
import sys      # interpreter variables access

if __name__ == '__main__':
    # Argument check
    if len(sys.argv) > 2:
        print("You have specified too many arguments")
        sys.exit()

    if len(sys.argv) < 2:
        print("You need to specify the path to be listed")
        sys.exit()

    # sys input passed argument check
    input_path = sys.argv[1]

    if not os.path.isdir(input_path):
        print("The path speficied does not exist")
        sys.exit()

    print("\n".join(os.listdir(input_path)))