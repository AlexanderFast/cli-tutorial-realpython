import argparse
import pathlib

my_parser = argparse.ArgumentParser(
    description='examples default values')

# default sets the the corresponding argument to default type, if
# argument is noct used in command line, ignores type conversion set
# though type

# type allows to interpret the argument to a python type
my_parser.add_argument('-int',
                       action='store',
                       type=int,
                       default=42,
                       help='%(default)i default value of optinal -int arg')
my_parser.add_argument('-float',
                       action='store',
                       type=float,
                       default=42.42,
                       help='%(default)f default value of optinal -int arg')
my_parser.add_argument('-ascii',
                       action='store',
                       type=ascii,
                       default='Baeumestarsse 8, 90762 Fuerth',
                       help='Any message can be stored')
my_parser.add_argument('-ord',
                       action='store',
                       type=ord,
                       default='A',
                       help='integer of unicode character')
# reading a file
my_parser.add_argument('-open_file',
                       action='store',
                       type=open,
                       default='src/deps/texts/type_open.txt',
                       help='Stores the file information')
# writing a file
my_parser.add_argument('-dest_file',
                       action='store',
                       default='src/deps/texts/dest_file.txt',
                       type=argparse.FileType('w', encoding='latin-1'),
                       help='creates a file under given path')
# using other imported types
my_parser.add_argument('-pathlib',
                       action='store',
                       type=pathlib.Path,
                       default='src/argsparser/',
                       help='transfers argiment to a patlib.Path object')

# user defined type


def hyphenated(string: str):
    return '-'.join([word[:4] for word in string.casefold().split()])


my_parser.add_argument('-user_typer',
                       action='store',
                       type=hyphenated,
                       default='The Tales of two Cities',
                       help='user defined type used')
                       
args = my_parser.parse_args()

print(vars(args))

# print what is written in the default file
# print(args.open_file.read())

# write to created file through cli
# args.dest_file.write('Information written to file')
