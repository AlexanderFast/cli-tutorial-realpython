import argparse

my_parser = argparse.ArgumentParser()
my_parser.version = '1.0'
my_parser.add_argument('-a',
                       action='store',
                       help='A will be stored')
my_parser.add_argument('-b',
                       action='store_const',
                       const=42,
                       help='%(const)i will be the value if used')
my_parser.add_argument('-c',
                       action='store_true',
                       help='true if used, else false')
my_parser.add_argument('-d',
                       action='store_false',
                       help='false if used, else true')
my_parser.add_argument('-e',
                       action='append',
                       help='appends E to list')
my_parser.add_argument('-f',
                       action='append_const',
                       const=42,
                       help='%(const)i appends to a list as often as used')
my_parser.add_argument('-g',
                       action='count',
                       help='counts how often it is used')
my_parser.add_argument('-i',
                       action='help',
                       help='same as -h')
my_parser.add_argument('-j', action='version')

args = my_parser.parse_args()

print(vars(args))
