import argparse

import os
import sys

if __name__ == '__main__':
    # Parser creation with description. Included in -h of command
    # Aguments define the help information
    # prog -> name of the parser
    # usage -> description in -h to explain how to use
    # description -> description what command will do
    my_parser = argparse.ArgumentParser(
        prog='myls',
        usage='%(prog)s [option] path',
        description='List the content of a folder',
        epilog='Text after help text :D',
        prefix_chars='-',   # default, jist shwoing
    )

    # definition of cli positional arguments, included in -h of command
    my_parser.add_argument('Path',
                           metavar='path',
                           type=str,
                           help='the path to list'
                           )
    my_parser.add_argument('-l',
                           '--long',
                           action='store_true',
                           help='enable the long listing format')

    # Execute the pars_args() method
    args = my_parser.parse_args()

    input_path = args.Path

    if not os.path.isdir(input_path):
        print('The path specified does not exist')
        sys.exit()

    # print('\n'.join(os.listdir(input_path)))

    for line in os.listdir(input_path):
        if args.long:  # Simplified long losting
            size = os.stat(os.path.join(input_path, line)).st_size
            line = '%10d  %s' % (size, line)
        print(line)
