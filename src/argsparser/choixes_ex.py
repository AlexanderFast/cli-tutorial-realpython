import argparse

my_parser = argparse.ArgumentParser()

my_parser.add_argument('-a',
                       action='store',
                       choices=['head', 'tail'])
my_parser.add_argument('-ir',
                       '--int_range',
                       action='store',
                       type=int,
                       choices=range(1, 6))


args = my_parser.parse_args()

print(vars(args))
