import argparse

my_parser = argparse.ArgumentParser(
    description='Abbrevation example',
    allow_abbrev=False,  # disable abbrevations
    )
# --input can be abbrevated linke --in, --inp, --inpu, if allow_abbrev=True
my_parser.add_argument('--input', action='store', type=int, required=True)
my_parser.add_argument('--id', action='store', type=int)

args = my_parser.parse_args()

print(args.input)
