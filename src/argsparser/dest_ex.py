import argparse

my_parser = argparse.ArgumentParser()

# name of property will be the first long name for optional agruments
# here: verbosity_level ('-' -> '_')
my_parser.add_argument('-v',
                       '--verbosity-level',
                       action='store',
                       type=int,
                       default=1)
# define property name with dest=
my_parser.add_argument('-vd',
                       '--verbosity',
                       action='store',
                       type=int,
                       default=2,
                       dest='my_verbosity_level')

args = my_parser.parse_args()

print(vars(args))
