import argparse

my_parser = argparse.ArgumentParser()

my_parser.add_argument('-a',
                       action='store',
                       choices=['head', 'tail'],
                       required=True)
# Bad practise for an optional argument to be required. If required use
# poritional agrument
args = my_parser.parse_args()

print(vars(args))