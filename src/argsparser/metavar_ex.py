import argparse

my_parser = argparse.ArgumentParser()

# metavar='<string>' is only used in the -h of cli to give more information
my_parser.add_argument('--foo', metavar='YYY')
my_parser.add_argument('bar', metavar='XXX')
my_parser.add_argument('-v',
                       '--verbosity',
                       action='store',
                       type=int,
                       metavar='LEVL')

args = my_parser.parse_args()

print(vars(args))
