import argparse

my_parser = argparse.ArgumentParser(
    description='Number of arguments (nargs) exampel.')

my_parser.add_argument('first',
                       action='store',
                       type=int,
                       nargs=3,
                       help='%(nargs)i agruments needed')
my_parser.add_argument('--input_1',
                       action='store',
                       nargs='?',
                       default='my default value',
                       help='single value, optional')
my_parser.add_argument('--input_2',
                       action='store',
                       nargs='*',
                       default='my default value',
                       help='flexible number of agruments -> list')
my_parser.add_argument('--input_3',
                       action='store',
                       nargs='+',
                       #    default='my default value',
                       help='at least one argument -> list')
my_parser.add_argument('others',
                       action='store',
                       nargs=argparse.REMAINDER,
                       #    default='my default value',
                       help='all remaining arguments -> list')

args = my_parser.parse_args()

print(vars(args))
